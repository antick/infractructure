﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Infractructure.Components.IO;
using Castle.Core.Logging;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace Antick.Infractructure.Tests
{
    public class TestClass
    {
        public double Value1 { get; set; }
        public DateTime Time { get; set; }
    }

   

    public class PackageTests
    {
        public static List<TestClass> TestData =
            new List<TestClass>
            {
                new TestClass {Value1 = 2.5, Time = DateTime.Now},
                new TestClass {Value1 = 10.44, Time = DateTime.Now.AddDays(-4)}
            };

        public static string Testpath = @"d:\tmp\___test_file__data.dat";


        public static void RemoveOld()
        {
            if (File.Exists(Testpath))
                File.Delete(Testpath);

            var dir = Path.GetDirectoryName(Testpath);

            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
        }

        /// <summary>
        /// Запись и чтение не упакованных данных
        /// </summary>
        [Test]
        public void WriteAndReadTest()
        {
            RemoveOld();

            var p = new ListPackage<TestClass>(new ConsoleLogger());

            p.Write(TestData, Testpath, PackageMode.UnPacked);

            var data = p.Read(Testpath, PackageMode.UnPacked);

            Assert.IsNotNull(data);
        }

        /// <summary>
        /// Запис и чтение упакованных данных
        /// </summary>
        [Test]
        public void WriteAndReadPackTest()
        {
            RemoveOld();

            var p = new ListPackage<TestClass>(new ConsoleLogger());

            p.Write(TestData, Testpath, PackageMode.PackedZip);

            var data = p.Read(Testpath, PackageMode.PackedZip);

            Assert.IsNotNull(data);
            Assert.IsTrue(TestData.First().Time == data.First().Time);
        }

        /// <summary>
        /// Конвертация в бинарные данные и чтение (без упаковки)
        /// </summary>
        [Test]
        public void ConvertAndRead()
        {
            RemoveOld();

            var p = new ListPackage<TestClass>(new ConsoleLogger());

            var data = p.ConvertToBase64(TestData, PackageMode.UnPacked);

            var dat = p.ReadFromBase64(data, PackageMode.UnPacked);

            Assert.IsNotNull(dat);
            Assert.IsTrue(TestData.First().Time == dat.First().Time);
        }

        /// <summary>
        /// Конвертация в бинарные данные и чтение (без упаковки)
        /// </summary>
        [Test]
        public void ConvertAndReadPacked()
        {
            RemoveOld();

            var p = new ListPackage<TestClass>(new ConsoleLogger());

            var data = p.ConvertToBase64(TestData, PackageMode.PackedZip);

            var dat = p.ReadFromBase64(data, PackageMode.PackedZip);

            Assert.IsNotNull(dat);
            Assert.IsTrue(TestData.First().Time == dat.First().Time);
        }

        /// <summary>
        /// Эмуляция работы Quote and RateManager. Запись в файл, чтение как бинарные, и десериализация обратно в данные
        /// </summary>
        [Test]
        public void FullTest()
        {
            RemoveOld();

            var p = new ListPackage<TestClass>(new ConsoleLogger());

            p.Write(TestData, Testpath, PackageMode.PackedZip);

            var data = p.ReadAsBase64(Testpath);

            var dat = p.ReadFromBase64(data, PackageMode.PackedZip);

            Assert.IsNotNull(dat);
            Assert.IsTrue(TestData.First().Time == dat.First().Time);
        }


        //[Ignore("fd")]
        [Test]
        public void ReadRealFile()
        {
            var p = new ListPackage<TestClass>(new ConsoleLogger());

            var base64 = p.ReadAsBase64(@"d:\tmp\test.dat");

            var dat = p.ReadFromBase64(base64, PackageMode.PackedZip);
        }
    }
}
