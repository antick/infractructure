﻿using System;
using System.Threading;

namespace Antick.Infractructure.Extensions
{
    public static class EventHandlerExt
    {
        /// <summary>
        /// Метод расширение, для организации потоко-безопасного вызова события.
        /// </summary>
        /// <typeparam name="TEventArgs"></typeparam>
        /// <param name="eventDelegate"></param>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public static void Raise<TEventArgs>(this EventHandler<TEventArgs> eventDelegate, object sender, TEventArgs args) where TEventArgs : EventArgs
        {
            EventHandler<TEventArgs> handler = Volatile.Read(ref eventDelegate);
            handler?.Invoke(sender, args);
        }

        public static void Raise<TEventArgs>(this Action<TEventArgs> eventDelegate, TEventArgs args)
        {
            Action<TEventArgs> handler = Volatile.Read(ref eventDelegate);
            handler?.Invoke(args);
        }

        public static void Raise(this Action eventDelegate)
        {
            Action handler = Volatile.Read(ref eventDelegate);
            handler?.Invoke();
        }
    }
}
