﻿using System;
using System.Threading.Tasks;

namespace Antick.Infractructure.Extensions
{
    public static class TaskExtensions
    {
        public static T GetSynchronousResult<T>(this Task<T> task)
        {
            //return task.GetAwaiter().GetResult();
            return Task.Run(async () => await task).ConfigureAwait(false).GetAwaiter().GetResult();
        }

        public static Task OnFail(this Task task, Action<Task, Exception> handler)
        {
            return task.ContinueWith(t =>
            {
                handler(t, t.Exception.GetBaseException());
            }, TaskContinuationOptions.OnlyOnFaulted);
        }
    }
}
