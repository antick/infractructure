using System.Collections.Generic;
using System.Linq;

namespace Antick.Infractructure.Extensions
{
    public static class ListExtensions
    {
        /// <summary>
        /// ���������� Null ��� ������ ������
        /// </summary>
        public static bool IsNullOrEmpty<T>(List<T> list) where  T : class
        {
            if (list == null) return true;
            return !list.Any();
        }
    }
}