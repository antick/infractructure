﻿using System;
using System.Collections.Generic;

namespace Antick.Infractructure.Extensions
{
    public static class DictionaryExtensions
    {
        public static TValue GetValueOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key)
        {
            return dictionary.GetValueOrDefault(key, () => default(TValue));
        }

        public static TValue GetValueOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, TValue defaultValue)
        {
            return dictionary.GetValueOrDefault(key, () => defaultValue);
        }

        public static TValue GetValueOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, Func<TValue> defaultValueFactory)
        {
            if (dictionary == null)
            {
                throw new ArgumentNullException("dictionary");
            }
            if (key == null)
            {
                throw new ArgumentNullException("key");
            }
            if (defaultValueFactory == null)
            {
                throw new ArgumentNullException("defaultValueFactory");
            }

            TValue value;
            if (!dictionary.TryGetValue(key, out value))
            {
                return defaultValueFactory();
            }

            return value;
        }

        public static void AddOrUpdate<TKey, TValue>(this Dictionary<TKey, TValue> dict, TKey key, TValue value)
        {
            if (dict.ContainsKey(key))
            {
                dict[key] = value;
            }
            else
            {
                dict.Add(key, value);
            }

        }
    }
}
