﻿using System;
using System.Windows.Forms;

namespace Antick.Infractructure.Extensions
{
    public static class ControlExtensions
    {
        /// <summary>
        /// Потоко безопасный вызов метода для контрола без ожидания завершения
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="c"></param>
        /// <param name="action"></param>
        public static void InvokeAsync<T>(this T c, Action<T> action)
            where T : Control
        {
            if (c.InvokeRequired)
            {
                c.Invoke(new Action(() => action(c)));
            }
            else
            {
                action(c);
            }
        }

        /// <summary>
        /// вызывает BeginInvoke и дожидается EndInvoke либо просто action если находится в своем потоке , а также блокирует контрол на время выполнения
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="c"></param>
        /// <param name="action"></param>
        public static void InvokeSync<T>(this T c, Action<T> action)
            where T : Control
        {
            if (c.InvokeRequired)
            {
                var result = c.BeginInvoke(new Action(() => action(c)));
                c.EndInvoke(result);
            }
            else
            {
                action(c);
            }
        }

        public static IAsyncResult BeginInvoke<T>(this T c, Action<T> action) where T : Control
        {
            return c.BeginInvoke(new Action(() => action(c)));
        }


        public static object InvokeSyncRet<T>(this T c, Func<object> action)
            where T : Control
        {
            if (c.InvokeRequired)
            {
                var result = c.BeginInvoke(action);
                var asyncResult = c.EndInvoke(result);
                return asyncResult;
            }
            return action();
        }
    }
}
