using System;
using System.Web;

namespace Antick.Infractructure.Extensions
{
    public static class DateTimeExtensions
    {
        /// <summary>
        /// �������������� ���� ��� ������� � �����
        /// </summary>
        public static string ToUtcUrlEncode(this DateTime time)
        {
            return HttpUtility.UrlEncode(time.ToUniversalTime().ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"));
        }
    }
}
