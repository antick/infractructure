﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Castle.Core.Logging;

namespace Antick.Infractructure.Components.Agent
{
    /// <summary>
    /// Агент, выполняющий работы в параллельном потоке, с какой то указанной периодичностью
    /// </summary>
    public abstract class BackAgent : IDisposable, IAgent
    {
        protected ILogger Logger;

        protected virtual TimeSpan ErrorDelayTime => new TimeSpan(0, 0, 1);

        private Task m_Task;
        private CancellationTokenSource m_CancellationTokenSource;


        protected BackAgent(ILogger logger)
        {
            Logger = logger;
        }

        public void Start()
        {
            if (m_Task != null) return;
            m_CancellationTokenSource = new CancellationTokenSource();

            try
            {
                m_Task = new Task(backgroundProccess, m_CancellationTokenSource.Token);
                m_Task.Start();
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat(ex, string.Format("Error in {0} Agent. Not started", GetType().Name));
            }
        }

        public void Stop()
        {
            if (m_Task != null)
            {
                m_CancellationTokenSource.Cancel();
                m_Task.Wait();
                m_CancellationTokenSource.Dispose();
                m_Task = null;
            }
        }

        public void Dispose()
        {
            Stop();
        }

        private void backgroundProccess()
        {
            while (!m_CancellationTokenSource.IsCancellationRequested)
            {
                try
                {
                    var delay = Execute();

                    // Выход если ответ реализован как null
                    if (delay == null)
                    {
                        Logger.DebugFormat("Agent {0}. Major back process is end", GetType().Name);
                        break;
                    }

                    if (delay.Value.TotalSeconds > 0)
                    {
                        Task.Delay(delay.Value, m_CancellationTokenSource.Token).Wait();
                    }
                }
                catch (Exception ex)
                {
                    Logger.ErrorFormat(ex, string.Format("Error in {0} Agent. Will be continue.", GetType().Name));
                    Logger.DebugFormat("Restart agent after {0} seconds",
                        ErrorDelayTime.TotalSeconds.ToString("0.00"));
                    Thread.Sleep(ErrorDelayTime);
                }
            }
        }

        protected virtual TimeSpan? Execute()
        {
            return null;
        }
    }
}
