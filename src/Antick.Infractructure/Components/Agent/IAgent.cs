﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Antick.Infractructure.Components.Agent
{
    public interface IAgent
    {
        void Start();
        void Stop();
    }
}
