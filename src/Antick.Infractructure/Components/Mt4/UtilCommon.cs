﻿using System;
using System.Globalization;

namespace Antick.Infractructure.Components.Mt4
{
    public static class UtilCommon
    {
        public static string MqlPeriodToPeriodStr(int period)
        {
            switch (period)
            {
                case (int)Constants.MqlPeriod.M1:
                    return "M1";
                case (int)Constants.MqlPeriod.M5:
                    return "M5";
                case (int)Constants.MqlPeriod.M15:
                    return "M15";
                case (int)Constants.MqlPeriod.M30:
                    return "M30";
                case (int)Constants.MqlPeriod.H1:
                    return "H1";
                case (int)Constants.MqlPeriod.H4:
                    return "H4";
                case (int)Constants.MqlPeriod.D1:
                    return "D1";
                case (int)Constants.MqlPeriod.W1:
                    return "W1";
                case (int)Constants.MqlPeriod.MN1:
                    return "MN1";
                default:
                    return "";
            }
        }

        public static DateTime Mt4DateTimeToDateTime(int mt4Time)
        {
            var Out = new DateTime(1970, 1, 1, 0, 0, 0);
            return Out.AddSeconds(mt4Time);
        }

        public static string ToStrMt4Format(DateTime date)
        {
            return date.ToString("yyyy.MM.dd hh:mm", CultureInfo.InvariantCulture);
        }
    }
}
