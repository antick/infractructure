﻿namespace Antick.Infractructure.Components.Mt4
{
    public static class Constants
    {
        public enum ErrorCodes
        {
            ServerError = -10,
            ServerSuccess = 10
        }

        /// <summary>
        /// Верхний разделитель
        /// </summary>
        public const string ArrayDelimeter = ";";
        /// <summary>
        /// Разделитель еще ниже - в сложном массиве.
        /// </summary>
        public const string ElementsDelimeter = ";";

        public enum MqlPeriod
        {
            M1 = 1,
            M5 = 5,
            M15 = 15,
            M30 = 30,
            H1 = 60,
            H4 = 240,
            D1 = 1440,
            W1 = 10080,
            MN1 = 43200
        }
    }
}
