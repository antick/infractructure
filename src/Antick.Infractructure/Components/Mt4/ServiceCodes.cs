﻿namespace Antick.Infractructure.Components.Mt4
{
    public enum ServiceCodes
    {
        // Клиентские коды ДЛЛ поставляемых на МТ4
        
        /// <summary>
        /// Процесс был запущен
        /// </summary>
        Started = 1,

        /// <summary>
        /// Процесс еще выполняется
        /// </summary>
        InProgress = 2,

        /// <summary>
        /// Процесс завершен
        /// </summary>
        Success = 3,

        /// <summary>
        /// Ошибка в ходе вызова Рест Сервиса
        /// </summary>
        RestTaskError = 4,

        UnknownError = 5,

        //---------------------------------------------
        
        // Успешный ответ с сервера
        ServerSuccess = 1000,

        /// <summary>
        /// Ошибка на стороне сервера
        /// </summary>
        ServerError = 1001,
      
    }
}
