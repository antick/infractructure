﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.Core.Logging;

namespace Antick.Infractructure.Components.IO
{
    public class ListPackage<T> : Package<List<T>>, IListPackage<T> where T: class
    {
        public ListPackage(ILogger logger) : base(logger)
        {
        }
    }
}
