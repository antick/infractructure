﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Antick.Infractructure.Components.IO
{
    public interface IPackage<T> where T: class
    {
        void Write(T data, string path, PackageMode mode = PackageMode.UnPacked);
        T Read(string path, PackageMode mode = PackageMode.UnPacked);
        string ReadAsBase64(string path);
        T ReadFromBase64(string data, PackageMode mode = PackageMode.UnPacked);
        string ConvertToBase64(T data, PackageMode mode = PackageMode.UnPacked);
    }
}
