﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Infractructure.Extensions;
using Castle.Core.Logging;
using Ionic.Zip;
using Newtonsoft.Json;

namespace Antick.Infractructure.Components.IO
{
    public class Package<T> : IPackage<T> where T: class, new() 
    {
        private readonly ILogger m_Logger;
        private const string ZIP_ITEM_NAME = "Content.bin";

        public Package(ILogger logger)
        {
            m_Logger = logger;
        }

        public void Write(T data, string path, PackageMode mode = PackageMode.UnPacked)
        {
            if (mode == PackageMode.UnPacked)
            {
                using (Stream stream = File.Open(path, FileMode.Create))
                {
                    using (var writer = new StreamWriter(stream))
                    {
                        var str = JsonConvert.SerializeObject(data);
                        writer.Write(str);
                    }
                }
            }
            else
            {
                using (var desc = File.Open(path, FileMode.Create))
                {
                    var str = JsonConvert.SerializeObject(data);
                    var stream = str.ToStream();
                    using (ZipFile zip = new ZipFile())
                    {
                        zip.AddEntry(ZIP_ITEM_NAME, stream);
                        zip.Save(desc);
                    }
                }
            }
        }

        public T Read(string path, PackageMode mode = PackageMode.UnPacked)
        {
            if (mode == PackageMode.UnPacked)
            {
                using (Stream stream = File.Open(path, FileMode.Open))
                {
                    using (var s = new StreamReader(stream))
                    {
                        var str = s.ReadToEnd();
                        try
                        {
                            return JsonConvert.DeserializeObject<T>(str);
                        }
                        catch (Exception e)
                        {
                            m_Logger.WarnFormat($"Error during reading hardCache from {path}. Return empty result");
                            m_Logger.Debug($"Detail error during reading hardCache from {path} : {e}");
                            return new T();
                        }

                    }
                }
            }
            else
            {
                using (var stream = File.Open(path, FileMode.Open))
                {
                    using (MemoryStream unZipStream = new MemoryStream())
                    {
                        // Распаковка данных в поток
                        using (ZipFile zip = ZipFile.Read(stream))
                        {
                            var entry = zip.Entries.First();
                            entry.Extract(unZipStream);
                        }
                        unZipStream.Position = 0;

                        // чтение данных из потока и дессериализация
                        using (var s = new StreamReader(unZipStream))
                        {
                            var str = s.ReadToEnd();
                            try
                            {
                                return JsonConvert.DeserializeObject<T>(str);
                            }
                            catch (Exception e)
                            {
                                m_Logger.WarnFormat($"Error during reading hardCache from stream. Return empty result");
                                m_Logger.Debug($"Detail error during reading hardCache from stream : {e}");
                                return new T();
                            }
                        }
                    }
                }
            }
        }

        public string ReadAsBase64(string path)
        {
            using (FileStream fs = new FileStream(path, FileMode.Open))
            {
                using (BinaryReader br = new BinaryReader(fs))
                {
                    byte[] bin = br.ReadBytes(Convert.ToInt32(fs.Length));
                    return Convert.ToBase64String(bin);
                }
            }
        }

        public T ReadFromBase64(string data, PackageMode mode = PackageMode.UnPacked)
        {
            var bytes = Convert.FromBase64String(data);

            if (mode == PackageMode.UnPacked)
            {
                using (var memoryStream = new MemoryStream(bytes))
                {
                    var s = new StreamReader(memoryStream);
                    var dataStr = s.ReadToEnd();
                    var str = JsonConvert.DeserializeObject<T>(dataStr);
                    return str;
                }
            }
            else
            {
                using (var memoryStream = new MemoryStream(bytes))
                {
                    var unZipStream = new MemoryStream();
                    using (ZipFile zip = ZipFile.Read(memoryStream))
                    {
                        var entry = zip.Entries.First();
                        entry.Extract(unZipStream);
                    }
                    unZipStream.Position = 0;

                    var s = new StreamReader(unZipStream);
                    var dataStr = s.ReadToEnd();
                    var str = JsonConvert.DeserializeObject<T>(dataStr);
                    return str;
                }
            }
        }

        public string ConvertToBase64(T data, PackageMode mode = PackageMode.UnPacked)
        {
            if (mode == PackageMode.UnPacked)
            {
                var str = JsonConvert.SerializeObject(data);
                var bytes = str.ToStream().ToArray();
                return Convert.ToBase64String(bytes);
            }
            else
            {
                var str = JsonConvert.SerializeObject(data);
                var bytes = str.ToStream().ToArray();

                using (var memoryStream = new MemoryStream(bytes))
                {
                    var packStream = new MemoryStream();
                    using (ZipFile zip = new ZipFile())
                    {
                        zip.AddEntry(ZIP_ITEM_NAME, memoryStream);
                        zip.Save(packStream);
                    }
                    packStream.Position = 0;

                    var array = packStream.ToArray();

                    return Convert.ToBase64String(array);
                }
            }
        }
    }
}
