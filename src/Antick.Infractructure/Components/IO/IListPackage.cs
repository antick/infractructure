﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Antick.Infractructure.Components.IO
{
    public interface IListPackage<T> where T : class
    {
        void Write(List<T> data, string path, PackageMode mode = PackageMode.UnPacked);
        List<T> Read(string path, PackageMode mode = PackageMode.UnPacked);
        string ReadAsBase64(string path);
        List<T> ReadFromBase64(string data, PackageMode mode = PackageMode.UnPacked);
        string ConvertToBase64(List<T> data, PackageMode mode = PackageMode.UnPacked);
    }
}
