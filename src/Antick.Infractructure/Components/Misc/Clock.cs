using System;

namespace Antick.Infractructure.Components.Misc
{
    public class Clock : IClock
    {
        public DateTime Now()
        {
            return DateTime.Now;
        }

        public DateTime UtcNow()
        {
            return DateTime.UtcNow;
        }
    }
}