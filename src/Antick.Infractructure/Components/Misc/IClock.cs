﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Antick.Infractructure.Components.Misc
{
    public interface IClock
    {
        DateTime Now();
        DateTime UtcNow();
    }
}
