﻿namespace Antick.Infractructure.Components.Mvp.Views
{
    public interface IView
    {
        void Show();
        void Close();
    }
}
