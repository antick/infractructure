﻿using Castle.MicroKernel;

namespace Antick.Infractructure.Components.Mvp.Presenters
{
    /// <summary>
    /// Корневой презентер, знает только о контроллере, и умеет запускаться через интерфейс
    /// </summary>
    public abstract class RootPresenter : IPresenter
    {
        protected IKernel Kernel { get; private set; }

        protected RootPresenter(IKernel kernel)
        {
            Kernel = kernel;
        }

        public virtual void Run()
        {
        }
    }
}
