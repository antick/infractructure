﻿using Antick.Infractructure.Components.Mvp.Views;
using Castle.MicroKernel;

namespace Antick.Infractructure.Components.Mvp.Presenters
{
    /// <summary>
    /// Первичный презентер, имеет еще View
    /// </summary>
    /// <typeparam name="TView"></typeparam>
    public abstract class ViewPresenter<TView> : RootPresenter
        where TView : IView
    {
        protected TView View { get; private set; }

        protected ViewPresenter(IKernel kernel, TView view)
            : base(kernel)
        {
            View = view;
        }

        public override void Run()
        {
            View.Show();
        }
    }
}
