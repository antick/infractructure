using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyTitle("Antick.Infractructure")]
[assembly: AssemblyDescription("Common infractrucute components")]
[assembly: AssemblyCompany("Antick")]
[assembly: AssemblyProduct("Antick.Infractructure")]
[assembly: AssemblyCopyright("Copyright � Antick 2017")]
[assembly: AssemblyTrademark("Antick.Infractructure")]
[assembly: AssemblyCulture("")]
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]